import urllib2
import re
import sys, argparse
import subprocess, shlex

#blocklist information

blocklists = {
	'abuse.ch Zeus Tracker (Domain)': {
		'id': 'abusezeusdomain',
		'url':  'https://zeustracker.abuse.ch/blocklist.php?download=squiddomain',
		'regex' : '',
		'file' : 'zeus_domain.acl',
	},
	'abuse.ch Zeus Tracker (IP)': {
		'id': 'abusezeusip',
		'url': 'https://zeustracker.abuse.ch/blocklist.php?download=squidip',
		'regex' : '',
		'file' : 'zeus_ip.acl',
	},
	'abuse.ch Palevo Tracker (Domain)': {
		'id': 'abusepalevodomain',
		'url':  'https://palevotracker.abuse.ch/blocklists.php?download=domainblocklist',
		'regex' : '',
		'file' : 'palevo_domain.acl',
	},
	'abuse.ch Palevo Tracker (IP)': {
		'id': 'abusepalevoip',
		'regex': '',
		'url':  'https://palevotracker.abuse.ch/blocklists.php?download=ipblocklist',
		'file': 'palevo_ip.acl',
	},
	'hpHosts ad-tracking servers': {
		'id': 'hphosts',
		'url': 'http://hosts-file.net/download/hosts.txt',
		'regex': 'Yes',
		'file' : 'hphosts_domain.acl',
	},
	'MVPS': {
		'id': 'mvps',
		'url': 'http://winhelp2002.mvps.org/hosts.txt',
		'regex': 'Yes',
		'file' : 'mvps.acl',
	},
	'malwaredomains.com Domain List': {
		'id': 'malwaredomainsdomain',
		'url': 'http://www.malwaredomainlist.com/hostslist/hosts.txt',
		'regex': 'Yes',
		'file' : 'malwaredomains.acl',
	},
	'Ransomware': {
		'id': 'ransomware',
		'url': 'https://ransomwaretracker.abuse.ch/downloads/RW_URLBL.txt',
		'regex': '',
		'file' : 'ransomware.acl',
	},
	'Stevenblack': {
		'id': 'stevenblack',
		'url': 'https://raw.githubusercontent.com/StevenBlack/hosts/master/hosts',
		'regex': '',
		'file' : 'stevenblack.acl',
	},
    'pgl.yoyo.org': {
        'id': 'pgl.yoyo.org',
        'url': 'http://pgl.yoyo.org/adservers/serverlist.php?hostformat=hosts&mimetype=plaintext',
        'regex': '',
        'file' : 'pgl.yoyo.org.acl',
        },
    'Hosts File Project': {
        'id': 'hostsfileproject',
        'url': 'http://hostsfile.mine.nu/Hosts',
        'regex': '',
        'file' : 'hfp.acl',
    },
    'The Cameleon Project': {
        'id': 'cameleonproject',
        'url': 'http://sysctl.org/cameleon/hosts',
        'regex': '',
        'file' : 'cameleon.acl',
    },
    'AdAway mobile ads': {
        'id': 'adaway',
        'url': 'http://adaway.sufficientlysecure.org/hosts.txt',
        'regex': '',
        'file' : 'adaway.acl',
    },
    'Someone Who Cares': {
        'id': 'someonewhocares',
        'url': 'http://someonewhocares.org/hosts/hosts',
        'regex': '',
        'file' : 'someonewhocares.acl',
    },
    #https://github.com/pi-hole/pi-hole/blob/master/adlists.default
    'pi-hole': {
        'id': 'pi-hole',
        'url': 'https://raw.githubusercontent.com/StevenBlack/hosts/master/hosts',
        'regex': '',
        'file' : 'pi-hole.acl',
    },
    'adblock': {
        'id': 'adblock',
        'url': 'http://adblock.gjtech.net/?format=unix-hosts',
        'regex': '',
        'file' : 'adblock.acl',
    },
    'disconnect-ad': {
        'id': 'disconnect-ad',
        'url': 'https://s3.amazonaws.com/lists.disconnect.me/simple_ad.txt',
        'regex': '',
        'file' : 'disconnect-ad.acl',
    },
    'disconnect-tracking': {
        'id': 'disconnect-tracking',
        'url': 'https://s3.amazonaws.com/lists.disconnect.me/simple_tracking.txt',
        'regex': '',
        'file' : 'disconnect-tracking.acl',
    },
    'Quidsups tracker list': {
        'id': 'quidsup',
        'url': 'https://raw.githubusercontent.com/quidsup/notrack/master/trackers.txt',
        'regex': '',
        'file' : 'quidsup.acl',
    },
    'Windows 10 telemetry list': {
        'id': 'wintelemetry',
        'url': 'https://raw.githubusercontent.com/crazy-max/WindowsSpyBlocker/master/data/hosts/win10/spy.txt',
        'regex': '',
        'file' : 'wintelemetry.acl',
    },
    'notracking': {
        'id': 'notracking',
        'url': 'https://raw.githubusercontent.com/notracking/hosts-blocklists/master/hostnames.txt',
        'regex': '',
        'file' : 'notracking.acl',
    },
    'Nothink.org Malware HTTP Traffic': {
        'id': 'nothinkhttp',
        'url': 'http://www.nothink.org/blacklist/blacklist_malware_http.txt',
        'regex' : '',
        'file' : 'nothinkhttp.acl',
     },
    'isc suspicious domains': {
        'id': 'iscdomains',
        'url': 'https://isc.sans.edu/feeds/suspiciousdomains_Medium.txt',
        'regex': '',
        'file' : 'isc-domains.acl',
    },
    'networksec': {
        'id': 'networksec',
        'url': 'http://www.networksec.org/grabbho/block.txt',
        'regex': '',
        'file' : 'networksec.acl',
    }

}

def downloadAndProcessBlocklist(url, regex, filename):
	req = urllib2.Request(url)
	req.add_header('User-Agent', 'Mozilla/5.0 (compatible; MSIE 8.0; Windows NT 6.1; Trident/5.0)')
	contents = ''
	
	#download blocklist
	try:
		response = urllib2.urlopen(req)
		contents = response.read()
		
		#process blocklists
		if regex == 'Yes':
			
			#remove comments, duplicates and process
			output = re.sub(r'(?m)^\#.*\n?', '', contents)
			listOutput = re.findall('(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+', output)	
			listOutput = list(set(listOutput))
            
            #remove whitelist
			listOutput = [x for x in listOutput if x != "127.0.0.1"]
			listOutput = [x for x in listOutput if x != "::1"]
			listOutput = [x for x in listOutput if x != "localhost"]
			listOutput = [x for x in listOutput if x != "s3-eu-west-1.amazonaws.com"]

			listOutput = [s+'\n' for s in listOutput]
			contents = ''.join(listOutput)
								
		
	except urllib2.URLError as e:
		if hasattr(e, 'reason'):
			print 'We failed to reach a server.'
			print 'Reason: ', e.reason
		elif hasattr(e, 'code'):
			print 'The server couldn\'t fulfill the request.'
			print 'Error code: ', e.code
		else:
			print 'unknown error'

	#write to file
	try:
		with open(location+filename, 'w') as f:
			f.write(str(contents))
			f.close()
	except IOError as e:
	  	print e.reason


def reloadSquidRules():
	print 'reloading squid blocklists'
	subprocess.check_call(shlex.split('/usr/local/sbin/squid -k reconfigure'))


# main

#sensible defaults
location = '/root/tables/squid/'

parser = argparse.ArgumentParser(description='IP blocklist downloader and importer for pf and ip tables')
parser.add_argument('-l', '--blocklist_location',help='location to store blocklists', required=False)
parser.add_argument('-n', '--blocklist_names',help='specify names of blocklists to download', required=False, type=lambda s: [str(item) for item in s.split(',')])

args = parser.parse_args()

if args.blocklist_location != None:
	location = args.blocklist_location


for key, value in sorted(blocklists.items()):

	#download all blocklists of the given type
	if args.blocklist_names == None:
		print('downloading '+key)
		downloadAndProcessBlocklist(value['url'], value['regex'], value['file'])
	else:
		#download specified blocklists
		if value['id'] in args.blocklist_names:
			print('downloading '+key)
			downloadAndProcessBlocklist(value['url'], value['regex'], value['file'])

reloadSquidRules
