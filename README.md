Squid-Blocklist-Downloader
====================

A python script to download squid blacklists and load them into squid

## Usage ##
-fw firewall type, currently pf is supported

-t blocklist type, currently ip, netblock, domain and all are supported

-l location to store blocklists 

-n specify names of blocklists to download

## Defaults ##
firewall = 'pf'

listType = 'ip'

location = '/root/tables/squid/'

## Misc ##
Please contact me with any suggestions or improvements.  I've made this for my own use, but thought others could benefit from it
